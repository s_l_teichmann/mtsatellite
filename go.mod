module bitbucket.org/s_l_teichmann/mtsatellite

go 1.20

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/bamiaux/rez v0.0.0-20170731184118-29f4463c688b
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/websocket v1.5.1
	github.com/jackc/pgx/v4 v4.18.1
	github.com/jmhodges/levigo v1.0.0
	github.com/klauspost/compress v1.17.4
	github.com/mattn/go-sqlite3 v1.14.19
	golang.org/x/crypto v0.17.0
)

require (
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.14.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.2 // indirect
	github.com/jackc/pgservicefile v0.0.0-20231201235250-de7065d80cb9 // indirect
	github.com/jackc/pgtype v1.14.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
