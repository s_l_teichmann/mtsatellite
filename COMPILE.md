#HOWTO compile MTSatellite

To build MTSatellite a [Go](http://golang.org) compiler 1.4 or better is needed.

Currently this is only tested on Debian Wheezy, Debian Jessie,
Ubuntu Ubuntu Trusty Thar (14.04) and newer. Other flavors
of GNU/Linux should work, too. Mac OS X may work. Problems with MS Windows
are expected.

A quick and dirty way to produce the binaries of `mtdbconverter`,
`mtredisalize`, `mtseeder` and `mtwebmapper`:

    # Assuming you have a 64bit GNU/Linux system. For other systems take
    # the corresponding version from https://golang.org/dl/
    $ wget https://storage.googleapis.com/golang/go1.10.linux-amd64.tar.gz

    $ echo "b5a64335f1490277b585832d1f6c7f8c6c11206cba5cd3f771dcb87b98ad1a33 go1.10.linux-amd64.tar.gz" | sha256sum -c -

    $ tar xf go1.10.linux-amd64.tar.gz

    $ mkdir -p gopath/{pkg,bin,src}

    $ export GOROOT=`pwd`/go

    $ export GOPATH=`pwd`/gopath

    $ export PATH=$GOROOT/bin:$GOPATH/bin:$PATH

    # On Debian Wheezy you have to install the LevelDB dev from Backports.
    $ sudo apt-get install libleveldb-dev

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtdbconverter

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtredisalize

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtseeder

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtwebmapper

    $ ls $GOPATH/bin
    mtdbconverter  mtredisalize  mtseeder  mtwebmapper

