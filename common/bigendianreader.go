// Copyright 2024 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

import (
	"encoding/binary"
	"io"
)

type bigEndianReader struct {
	parent io.Reader
	buf    [4]byte
	err    error
}

func (ber *bigEndianReader) u8() (uint8, error) {
	if ber.err != nil {
		return 0, ber.err
	}
	_, err := io.ReadFull(ber.parent, ber.buf[:1])
	if err != nil {
		ber.err = err
		return 0, err
	}
	return ber.buf[0], nil
}

func (ber *bigEndianReader) u16() (uint16, error) {
	if ber.err != nil {
		return 0, ber.err
	}
	two := ber.buf[:2]
	_, err := io.ReadFull(ber.parent, two)
	if err != nil {
		ber.err = err
		return 0, err
	}
	return binary.BigEndian.Uint16(two), nil
}

func (ber *bigEndianReader) u32() (uint32, error) {
	if ber.err != nil {
		return 0, ber.err
	}
	four := ber.buf[:4]
	_, err := io.ReadFull(ber.parent, four)
	if err != nil {
		ber.err = err
		return 0, err
	}
	return binary.BigEndian.Uint32(four), nil
}

func (ber *bigEndianReader) str(n int) (string, error) {
	if ber.err != nil {
		return "", ber.err
	}
	buf := make([]byte, n)
	_, err := io.ReadFull(ber.parent, buf)
	if err != nil {
		ber.err = err
		return "", err
	}
	return string(buf), nil
}

func (ber *bigEndianReader) full(n int) ([]byte, error) {
	if ber.err != nil {
		return nil, ber.err
	}
	buf := make([]byte, n)
	_, err := io.ReadFull(ber.parent, buf)
	if err != nil {
		ber.err = err
		return nil, err
	}
	return buf, nil
}
