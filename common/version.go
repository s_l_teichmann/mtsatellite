// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

import (
	"fmt"
	"os"
)

const MTSatelliteVersion = "0.9.1"

func PrintVersionAndExit() {
	fmt.Printf("Version: %s\n", MTSatelliteVersion)
	os.Exit(0)
}
