// Copyright 2014, 2015, 2017 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

import "container/heap"

// YOrder is a "streaming" Y sorter. The blocks comming from the
// database are not sorted in Y order. To unpack only the
// relevant blocks (the one at the surface) it would be nice
// to have them sorted in inverse Y order so that blocks with
// lower Y value are shadowed by ones wither higher value.
//
// Sorting all blocks correctly would leadind to load all blocks
// before rendering. But a perfect order is not strictly necessary
// because the problem is (expensively) solved at per node level.
//
// The YOrder defines a "windowed" data structure in which all blocks
// are sorted correctly. So for small amounts of blocks the
// sorting is perfect. For larger amounts it is possible to
// have partial incorrect sortings but as stated above it doesn't
// matter. The window allows not to preload all blocks.

type YOrder struct {
	RenderFn func(*Block) error
	blocks   []*Block
	capacity int
}

func NewYOrder(renderFn func(*Block) error, capacity int) *YOrder {
	return &YOrder{
		RenderFn: renderFn,
		blocks:   make([]*Block, 0, capacity),
		capacity: capacity}
}

func (yo *YOrder) Reset() {
	blocks := yo.blocks
	for i := range blocks {
		blocks[i] = nil
	}
	yo.blocks = blocks[:0]
}

func (yo *YOrder) RenderBlock(block *Block) (*Block, error) {
	if len(yo.blocks) == yo.capacity {
		oblock := yo.blocks[0]
		if oblock.Coord.Y < block.Coord.Y {
			// New one is above highest old. Directly render new.
			err := yo.RenderFn(block)
			return block, err
		}
		// Render old one. Store copy of new in heap.
		heap.Pop(yo)
		heap.Push(yo, block)
		err := yo.RenderFn(oblock)
		return oblock, err
	}

	heap.Push(yo, block)
	return nil, nil
}

func (yo *YOrder) Drain() error {
	for len(yo.blocks) > 0 {
		if err := yo.RenderFn(heap.Pop(yo).(*Block)); err != nil {
			return err
		}
	}
	return nil
}

func (yo *YOrder) Len() int {
	return len(yo.blocks)
}

func (yo *YOrder) Swap(i, j int) {
	yo.blocks[i], yo.blocks[j] = yo.blocks[j], yo.blocks[i]
}

func (yo *YOrder) Less(i, j int) bool {
	// Reverse order intented.
	return yo.blocks[i].Coord.Y > yo.blocks[j].Coord.Y
}

func (yo *YOrder) Push(x interface{}) {
	yo.blocks = append(yo.blocks, x.(*Block))
}

func (yo *YOrder) Pop() interface{} {
	blocks := yo.blocks
	l := len(blocks)
	x := blocks[l-1]
	blocks[l-1] = nil
	yo.blocks = blocks[:l-1]
	return x
}
