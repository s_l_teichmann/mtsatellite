// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max32(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

func max16(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

func min16(a, b int16) int16 {
	if a < b {
		return a
	}
	return b
}

func min32f(a, b float32) float32 {
	if a < b {
		return a
	}
	return b
}

func Clamp32f(x, a, b float32) float32 {
	switch {
	case x < a:
		return a
	case x > b:
		return b
	}
	return x
}

func Order(a, b int) (int, int) {
	if a < b {
		return a, b
	}
	return b, a
}

func Order16(a, b int16) (int16, int16) {
	if a < b {
		return a, b
	}
	return b, a
}

func Order64(a, b int64) (int64, int64) {
	if a < b {
		return a, b
	}
	return b, a
}
