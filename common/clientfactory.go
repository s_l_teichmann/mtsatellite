// Copyright 2022 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

import (
	"strings"
)

type DBClient interface {
	QueryCuboid(cuboid Cuboid, fn func(*Block) *Block) (count int, err error)
	Close() error
}

type DBClientFactory interface {
	Create() (DBClient, error)
	Close() error
}

func IsPostgreSQL(host string) (string, bool) {
	if strings.HasPrefix(host, "postgres:") {
		return host[len("postgres:"):], true
	}
	return "", false
}

func CreateDBClientFactory(host string, port int) (DBClientFactory, error) {

	if connS, ok := IsPostgreSQL(host); ok {
		return NewPGClientFactory(connS)
	}

	return NewRedisClientFactory(host, port)
}
