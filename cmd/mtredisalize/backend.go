// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

type (
	// block is the essential transfer unit from to the database.
	// Key is the serialized spatial position.
	// Data is the serialized from of the corresponding block data.
	block struct {
		Key  []byte
		Data []byte
	}

	// session is a database session.
	session interface {
		// del deletes a block by a given key.
		del(hash, key []byte) (bool, error)
		// fetch fetches the block data for a given position.
		fetch(hash, key []byte) ([]byte, error)
		// inTransaction returns true if a transaction is running.
		inTransaction() bool
		// store stores a block with a given position and data.
		store(hash, key, value []byte) (bool, error)
		// allKeys returns all keys in the database.
		allKeys(hash []byte, done <-chan struct{}) (<-chan []byte, int, error)
		// spatialQuery performs a box query between the positions first and second.
		spatialQuery(hash, first, second []byte, done <-chan struct{}) (<-chan block, error)
		// beginTransaction starts a transcation.
		beginTransaction() error
		// commitTransaction finishes a transaction.
		commitTransaction() error
		// close closes the database session.
		close() error
	}

	// backend is the interface representing a database.
	backend interface {
		// newSession opens a new session.
		newSession() (session, error)
		// shutdown shuts down the database server.
		shutdown() error
	}
)
