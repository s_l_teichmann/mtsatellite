# Minetest Web-Map-Client

The web map client for mtsatellite is build as vue app using vuetify and leaflet as frameworks.
The following commands are all you need to set this application up.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
The result of this command is a ```dist``` folder containing the minified version. To use this in production, copy or move the folder to the location of your choice to serve the app.

### Lints and fixes files
```
yarn lint
```

### Customize configuration
To configure the backend websocket URL and the app title adjust the entries in the ```.env``` file.

See [Configuration Reference](https://cli.vuejs.org/config/).
