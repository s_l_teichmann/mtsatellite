// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"

	"bitbucket.org/s_l_teichmann/mtsatellite/common"

	"github.com/gorilla/mux"
)

func main() {

	var (
		cfg     config
		cfgFile string
		version bool
	)

	flag.StringVar(&cfgFile, "config", "", "configuration file")
	flag.BoolVar(&version, "version", false, "Print version and exit.")
	cfg.bindFlags()

	flag.Parse()

	if version {
		common.PrintVersionAndExit()
	}

	if cfgFile != "" {
		if err := cfg.load(cfgFile); err != nil {
			log.Fatalf("error: %v\n", err)
		}
	}

	bg := common.ParseColorDefault(cfg.BGColor, common.BackgroundColor)

	router := mux.NewRouter()

	subBaseLine := newSubBaseLine(cfg.MapDir, bg)
	router.Path("/map/{z:[0-9]+}/{x:[0-9]+}/{y:[0-9]+}.png").Handler(subBaseLine)

	var btu baseTilesUpdates
	var wsf *websocketForwarder

	if cfg.Websockets {
		wsf = newWebsocketForwarder()
		go wsf.run()
		router.Path("/ws").Methods("GET").Handler(wsf)
		btu = wsf
	}

	if cfg.PlayersFIFO != "" {
		plys := newPlayers(cfg.PlayersFIFO, wsf)
		wsf.setInit(plys.initConnection)
		go plys.run()

		router.Path("/players").Methods("GET").Handler(plys)
	}

	if cfg.RedisHost != "" {

		var colors *common.Colors
		var err error
		if colors, err = common.ParseColors(cfg.ColorsFile); err != nil {
			log.Fatalf("ERROR: problem loading colors: %s", err)
		}
		colors.TransparentDim = common.Clamp32f(
			float32(cfg.TransparentDim/100.0), 0.0, 100.0)

		dbcf, err := common.CreateDBClientFactory(cfg.RedisHost, cfg.RedisPort)
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}
		defer dbcf.Close()

		var allowedUpdateIps []net.IP
		if allowedUpdateIps, err = ipsFromHosts(cfg.UpdateHosts); err != nil {
			log.Fatalf("ERROR: name resolving problem: %s", err)
		}

		tu := newTileUpdater(
			cfg.MapDir,
			dbcf,
			allowedUpdateIps,
			colors, bg,
			cfg.YMin, cfg.YMax,
			cfg.Transparent,
			cfg.Workers,
			btu)
		go tu.doUpdates()

		if pgHost, ok := common.IsPostgreSQL(cfg.RedisHost); btu != nil && ok {
			go tu.listen(pgHost, cfg.ChangeDuration.Duration)
		} else {
			router.Path("/update").Methods("POST").Handler(tu)
		}
	}

	router.PathPrefix("/").Handler(http.FileServer(http.Dir(cfg.WebDir)))
	http.Handle("/", router)

	addr := fmt.Sprintf("%s:%d", cfg.WebHost, cfg.WebPort)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatalf("Starting server failed: %s\n", err)
	}
}
