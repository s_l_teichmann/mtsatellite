// Copyright 2014, 2015, 2022 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"time"

	"github.com/BurntSushi/toml"

	"bitbucket.org/s_l_teichmann/mtsatellite/common"
)

type duration struct {
	time.Duration
}

type config struct {
	WebPort        int      `toml:"web_port"`
	WebHost        string   `toml:"web_host"`
	WebDir         string   `toml:"web"`
	MapDir         string   `toml:"map"`
	RedisPort      int      `toml:"redis_port"`
	RedisHost      string   `toml:"redis_host"`
	ColorsFile     string   `toml:"colors"`
	BGColor        string   `toml:"background"`
	Workers        int      `toml:"workers"`
	Transparent    bool     `toml:"transparent"`
	TransparentDim float64  `toml:"transparent_dim"`
	UpdateHosts    string   `toml:"update_hosts"`
	Websockets     bool     `toml:"websockets"`
	PlayersFIFO    string   `toml:"players"`
	YMin           int      `toml:"ymin"`
	YMax           int      `toml:"ymax"`
	ChangeDuration duration `toml:"change_duration"`
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

func (cfg *config) bindFlags() {
	defaultBgColor := common.ColorToHex(common.BackgroundColor)

	flag.IntVar(&cfg.WebPort, "web-port", 8808, "port of the web server")
	flag.IntVar(&cfg.WebPort, "p", 8808, "port of the web server (shorthand)")
	flag.StringVar(&cfg.WebHost, "web-host", "localhost", "address to bind web server")
	flag.StringVar(&cfg.WebHost, "h", "localhost", "address to bind web server(shorthand)")
	flag.StringVar(&cfg.WebDir, "web", "web", "static served web files.")
	flag.StringVar(&cfg.WebDir, "w", "web", "static served web files (shorthand)")
	flag.StringVar(&cfg.MapDir, "map", "map", "directory of prerendered tiles")
	flag.StringVar(&cfg.MapDir, "m", "map", "directory of prerendered tiles (shorthand)")
	flag.StringVar(&cfg.UpdateHosts, "update-hosts", "localhost",
		"';' separated list of hosts which are allowed to send map update requests")
	flag.StringVar(&cfg.UpdateHosts, "u", "localhost",
		"';' separated list of hosts which are allowed to send map update requests (shorthand)")
	flag.StringVar(&cfg.RedisHost, "redis-host", "", "address of the backend Redis server")
	flag.StringVar(&cfg.RedisHost, "rh", "", "address of the backend Redis server (shorthand)")
	flag.IntVar(&cfg.RedisPort, "redis-port", 6379, "port of the backend Redis server")
	flag.IntVar(&cfg.RedisPort, "rp", 6379, "port of the backend Redis server (shorthand)")
	flag.IntVar(&cfg.Workers, "workers", 1, "number of workers to render tiles")
	flag.StringVar(&cfg.ColorsFile, "colors", "colors.txt", "colors used to render map tiles.")
	flag.StringVar(&cfg.ColorsFile, "c", "colors.txt", "colors used to render map tiles (shorthand).")
	flag.StringVar(&cfg.BGColor, "background", defaultBgColor, "background color")
	flag.StringVar(&cfg.BGColor, "bg", defaultBgColor, "background color (shorthand)")
	flag.BoolVar(&cfg.Transparent, "transparent", false, "Render transparent blocks.")
	flag.BoolVar(&cfg.Transparent, "t", false, "Render transparent blocks (shorthand).")
	flag.Float64Var(&cfg.TransparentDim,
		"transparent-dim", common.DefaultTransparentDim*100.0,
		"Extra dimming of transparent nodes each depth meter in percent.")
	flag.Float64Var(&cfg.TransparentDim,
		"td", common.DefaultTransparentDim*100.0,
		"Extra dimming of transparent nodes each depth meter in percent. (shorthand)")
	flag.BoolVar(&cfg.Websockets, "websockets", false, "Forward tile changes to clients via websockets.")
	flag.BoolVar(&cfg.Websockets, "ws", false, "Forward tile changes to clients via websockets (shorthand).")
	flag.StringVar(&cfg.PlayersFIFO, "players", "", "Path to FIFO file to read active players from.")
	flag.StringVar(&cfg.PlayersFIFO, "ps", "", "Path to FIFO file to read active players from (shorthand).")
	flag.IntVar(&cfg.YMin, "ymin", common.MinHeight, "Minimum y in blocks.")
	flag.IntVar(&cfg.YMax, "ymax", common.MaxHeight, "Maximum y in blocks.")
	flag.DurationVar(&cfg.ChangeDuration.Duration,
		"change-duration", time.Second, "Duration to aggregate changes. (PG only)")
}

func (cfg *config) load(fname string) error {
	_, err := toml.DecodeFile(fname, &cfg)
	return err
}
