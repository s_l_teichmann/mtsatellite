// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"

	"bitbucket.org/s_l_teichmann/mtsatellite/common"
)

func main() {
	var (
		port             int
		host             string
		xMin, yMin, zMin int
		xMax, yMax, zMax int
		colorsFile       string
		bgColor          string
		outDir           string
		numWorkers       int
		skipBaseLevel    bool
		skipPyramid      bool
		transparent      bool
		transparentDim   float64
		version          bool
	)

	defaultBgColor := common.ColorToHex(common.BackgroundColor)

	flag.IntVar(&port, "port", 6379, "port to of mtredisalize server")
	flag.IntVar(&port, "p", 6379, "port to of mtredisalize server (shorthand)")
	flag.StringVar(&host, "host", "localhost", "host to mtredisalize server")
	flag.IntVar(&xMin, "xmin", -1933, "x min of the area to tile")
	flag.IntVar(&xMax, "xmax", 1932, "x max of the area to tile")
	flag.IntVar(&yMin, "ymin", common.MinHeight, "Minimum y in blocks.")
	flag.IntVar(&yMax, "ymax", common.MaxHeight, "Maximum y in blocks.")
	flag.IntVar(&zMin, "zmin", -1933, "z min of the area to tile")
	flag.IntVar(&zMax, "zmax", 1932, "z max of the area to tile")
	flag.StringVar(&colorsFile, "colors", "colors.txt", "definition of colors")
	flag.StringVar(&bgColor, "background", defaultBgColor, "background color")
	flag.StringVar(&bgColor, "bg", defaultBgColor, "background color (shorthand)")
	flag.StringVar(&outDir, "output-dir", "map", "directory with the resulting image tree")
	flag.StringVar(&outDir, "o", "map", "directory with the resulting image tree")
	flag.IntVar(&numWorkers, "workers", 1, "number of workers")
	flag.IntVar(&numWorkers, "w", 1, "number of workers (shorthand)")
	flag.BoolVar(&skipBaseLevel, "skip-base-level", false, "Do not generate base level tiles")
	flag.BoolVar(&skipBaseLevel, "sb", false, "Do not generate base level tiles (shorthand)")
	flag.BoolVar(&skipPyramid, "skip-pyramid", false, "Do not generate pyramid tiles")
	flag.BoolVar(&skipPyramid, "sp", false, "Do not generate pyramid tiles (shorthand)")
	flag.BoolVar(&transparent, "transparent", false, "Render transparent blocks.")
	flag.BoolVar(&transparent, "t", false, "Render transparent blocks (shorthand).")
	flag.Float64Var(&transparentDim,
		"transparent-dim", common.DefaultTransparentDim*100.0,
		"Extra dimming of transparent nodes each depth meter in percent.")
	flag.Float64Var(&transparentDim,
		"td", common.DefaultTransparentDim*100.0,
		"Extra fimming of transparent nodes each depth meter in percent. (shorthand)")
	flag.BoolVar(&version, "version", false, "Print version and exit.")

	flag.Parse()

	if version {
		common.PrintVersionAndExit()
	}

	bg := common.ParseColorDefault(bgColor, common.BackgroundColor)

	dbcf, err := common.CreateDBClientFactory(host, port)
	if err != nil {
		log.Fatalf("error: %s\n", err)
	}
	defer dbcf.Close()

	if !skipBaseLevel {
		td := common.Clamp32f(float32(transparentDim/100.0), 0.0, 1.0)
		if err := createBaseLevel(
			dbcf,
			xMin, yMin, zMin, xMax, yMax, zMax,
			transparent, td,
			colorsFile, bg,
			outDir,
			numWorkers); err != nil {
			log.Fatalf("Creating base level tiles failed: %s", err)
		}
	}
	if !skipPyramid {
		pc := pyramidCreator{numWorkers: numWorkers, outDir: outDir, bg: bg}
		if err := pc.create(); err != nil {
			log.Fatalf("Creating pyramid tiles failed: %s", err)
		}
	}
}
