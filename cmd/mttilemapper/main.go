// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"image"
	"log"
	"os"
	"runtime/pprof"

	"bitbucket.org/s_l_teichmann/mtsatellite/common"
)

func main() {
	var (
		port                 int
		host                 string
		x, y, z              int
		width, height, depth int
		colorsfile           string
		bgColor              string
		outfile              string
		shaded               bool
		transparent          bool
		cpuProfile           string
		transparentDim       float64
		version              bool
	)

	defaultBgColor := common.ColorToHex(common.BackgroundColor)

	flag.IntVar(&port, "port", 6379, "port to of mtredisalize server")
	flag.IntVar(&port, "p", 6379, "port to of mtredisalize server (shorthand)")
	flag.StringVar(&host, "host", "localhost", "host to mtredisalize server")
	flag.IntVar(&x, "x", 0, "x of query cuboid")
	flag.IntVar(&y, "y", -75, "y of query cuboid")
	flag.IntVar(&z, "z", 0, "z of query cuboid")
	flag.IntVar(&width, "width", 16, "width of query cuboid")
	flag.IntVar(&height, "height", 16, "height of query cuboid")
	flag.IntVar(&depth, "depth", 150, "depth of query cuboid")
	flag.IntVar(&width, "w", 16, "width of query cuboid (shorthand)")
	flag.IntVar(&height, "h", 16, "height of query cuboid (shorthand)")
	flag.IntVar(&depth, "d", 150, "depth of query cuboid (shorthand)")
	flag.StringVar(&colorsfile, "colors", "colors.txt", "definition of colors")
	flag.StringVar(&bgColor, "background", defaultBgColor, "background color")
	flag.StringVar(&bgColor, "bg", defaultBgColor, "background color (shorthand)")
	flag.StringVar(&outfile, "output", "out.png", "image file of result")
	flag.StringVar(&outfile, "o", "out.png", "image file of result (shorthand)")
	flag.BoolVar(&shaded, "shaded", true, "draw relief")
	flag.BoolVar(&transparent, "transparent", false, "render transparent blocks")
	flag.Float64Var(
		&transparentDim, "transparent-dim", common.DefaultTransparentDim*100,
		"Extra dimming of transparent nodes every depth meter in percent (0-100).")
	flag.StringVar(&cpuProfile, "cpuprofile", "", "write cpu profile to file")
	flag.BoolVar(&version, "version", false, "Print version and exit.")

	flag.Parse()

	if version {
		common.PrintVersionAndExit()
	}

	bg := common.ParseColorDefault(bgColor, common.BackgroundColor)

	if cpuProfile != "" {
		f, err := os.Create(cpuProfile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	var colors *common.Colors
	var err error
	if colors, err = common.ParseColors(colorsfile); err != nil {
		log.Fatalf("Cannot open color file: %s", err)
	}

	colors.TransparentDim = common.Clamp32f(
		float32(transparentDim/100.0), 0.0, 100.0)

	cf, err := common.CreateDBClientFactory(host, port)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	client, err := cf.Create()
	if err != nil {
		log.Fatalf("Cannot connect to '%s:%d': %s", host, port, err)
	}
	defer client.Close()

	if shaded {
		width += 2
		height += 2
		x--
		z--
	}

	q1x, q1y, q1z := int16(x), int16(y), int16(z)
	q2x, q2y, q2z := q1x+int16(width)-1, q1y+int16(depth)-1, q1z+int16(height)-1

	renderer := common.NewRenderer(width, height, transparent)
	renderer.SetPos(q1x, q1z)

	renderFn := func(block *common.Block) error {
		return renderer.RenderBlock(block, colors)
	}

	yOrder := common.NewYOrder(renderFn, 512)

	numBlocks := 0
	drawBlock := func(block *common.Block) *common.Block {
		block, err := yOrder.RenderBlock(block)
		if err != nil {
			log.Printf("WARN: rendering block failed: %s\n", err)
		}
		numBlocks++
		return block
	}

	c1 := common.Coord{X: q1x, Z: q1z}
	c2 := common.Coord{X: q2x, Z: q2z}
	for c2.Y = q2y; c2.Y > q1y; c2.Y -= 8 {
		c1.Y = c2.Y - 7
		if c1.Y < q1y {
			c1.Y = q1y
		}
		cuboid := common.Cuboid{P1: common.MinCoord(c1, c2), P2: common.MaxCoord(c1, c2)}
		if _, err = client.QueryCuboid(cuboid, drawBlock); err != nil {
			log.Fatalf("query failed: %s", err)
		}
		if err = yOrder.Drain(); err != nil {
			log.Printf("WARN: rendering block failed: %s\n", err)
		}
		if renderer.IsFilled() {
			break
		}
	}

	var image image.Image

	if shaded {
		image = renderer.CreateShadedImage(
			16, 16, (width-2)*16, (height-2)*16,
			colors, bg)
	} else {
		image = renderer.CreateImage(colors.Colors, bg)
	}

	if err = common.SaveAsPNG(outfile, image); err != nil {
		log.Fatalf("writing image failed: %s", err)
	}

	log.Printf("num blocks: %d\n", numBlocks)
	log.Printf("rejected blocks: %d\n", renderer.RejectedBlocks)
	log.Printf("transparent blocks: %d\n", renderer.TransparentBlocks)
	log.Printf("solid blocks: %d\n", renderer.SolidBlocks)
}
