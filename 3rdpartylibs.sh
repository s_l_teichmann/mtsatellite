#!/bin/sh
# Currently used third party libraries
go get -u -v golang.org/x/crypto/blake2b
go get -u -v github.com/bamiaux/rez
go get -u -v github.com/jmhodges/levigo
go get -u -v github.com/mattn/go-sqlite3
go get -u -v github.com/gorilla/mux
go get -u -v github.com/gorilla/websocket
