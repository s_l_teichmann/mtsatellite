BEGIN;

  CREATE OR REPLACE FUNCTION send_block_changes()
  RETURNS TRIGGER AS
$$
BEGIN
  PERFORM pg_notify('blockchanges',
    json_build_object(
      'X', NEW.posx,
      'Z', NEW.posz
    )::text
  );
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER blocks_changed
  AFTER INSERT OR UPDATE
  ON blocks
  FOR EACH ROW
  EXECUTE PROCEDURE send_block_changes();

END;
